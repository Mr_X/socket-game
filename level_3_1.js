let createLevel_3_1=()=>{
    disbtn=true;
    let root=document.getElementById("root");
    root.innerHHTML='';
    level_game=3;
}
let level_3_object_1_is=true;
let level_3_object_1;
let level_3_object_2_is=true;
let level_3_object_2;
let level_3_object_3_is=true;
let level_3_object_3;
let level_3_object_4_is=true;
let level_3_object_4;

let click_3_3=()=>{
    node=document.getElementById("level_3_object_3");
    node.style.left="calc(100% - 50px)";
    node.style.top="170px";
    node.style.width=0;
    node.style.height=0;
    if(level_3_object_2_is){
        setBag("Чтобы найти частоту встречаемости символа нужно найти отношение количества этих символов в тексте к общему количеству символов.");
        funcEval('level_3_object_3_is=false');
        funcEval('level_3_object_3.onclick()');
    }
};
let click_3_2=()=>{
    node=document.getElementById("level_3_object_2");
    node.style.left="calc(100% - 50px)";
    node.style.top="170px";
    node.style.width=0;
    node.style.height=0;
    if(level_3_object_2_is){
        setBag("Стоит обратить внимание на частоту появления сочетаний двух букв в словах русского языка. Самое частое сочетание – ТЬ, второе по частоте сочетание – АТ, третье – СТ. Если в зашифрованном тексте часто встречаются одинаковые сочетания, можно предположить, что ими были заменены сочетания ТЬ, АТ, СТ.");
        funcEval('level_3_object_2_is=false');
        funcEval('level_3_object_2.onclick()');
    }
};
let click_3_1=()=>{
    node=document.getElementById("level_3_object_1");
    node.style.left="calc(100% - 50px)";
    node.style.top="170px";
    node.style.width=0;
    node.style.height=0;
    if(level_3_object_1_is){
        setBag("Определить осмысленность текста можно на основе статистического анализа частот появления букв. Буквы в языках встречаются с разной частотой. Вероятность появления в русскоязычном тексте буквы «А» – 9,2%. Она встречается чаще других.<img src='/table' / >");
        funcEval('level_3_object_1_is=false');
        funcEval('level_3_object_1.onclick()');
    }
};


let click_3_4=()=>{
    node=document.getElementById("level_3_object_4");
    node.style.left="calc(100% - 50px)";
    node.style.top="170px";
    node.style.width=0;
    node.style.height=0;
    if(level_3_object_4_is){
        setBag(`!р?в#т, $#з$%к*=цы.

=ы к*л*$?+ты !#рв*й эк+!#д?ц??. Ш#+ть =#+яц#в $%з%д =ы д#рж%л? !уть к ч#тв#рт*й !л%$#т# +*зв#зд?я К?т% — Т%у. К +*ж%л#$?ю, =?++?я $# !р*шл% у+!#ш$*. $%= !р?шл*+ь +*в#рш?@ь эк+@р#$$ую !*+%дку. ?з-з% !*л*=к? к*р%)ля !*+@р%д%л% )*льш%я ч%+@ь $%ш#г* эк?!%ж% ? $%= +р*ч$* $уж$% !*=*щь. $%ш? к**рд?$%@ы: $*ль $*ль ч#@ыр# дв% *д?$$%дц%@ь.`);
funcEval('level_3_object_4_is=false');
funcEval('level_3_object_4.onclick()');
}
};



let createLevel_3=()=>{
    document.body.style.backgroundImage="url(/t5)";
    document.getElementById("root").innerHTML="";
    level_game=2;
    if(level_3_object_1_is){
        level_3_object_1=document.createElement('button');
        level_3_object_1.style.left="34%";
        level_3_object_1.style.top="57%";
        level_3_object_1.innerHTML='3 1';
        level_3_object_1.className="btn-question";
        level_3_object_1.id="level_3_object_1";
        document.getElementById("root").appendChild(level_3_object_1);
        level_3_object_1.onclick=click_3_1;
    }
    
    if(level_3_object_2_is){
        level_3_object_2=document.createElement('button');
        level_3_object_2.style.left="57%";
        level_3_object_2.style.top="34%";
        level_3_object_2.className="btn-question";
        level_3_object_2.id="level_3_object_2";
        level_3_object_2.innerHTML='3 2';
        document.getElementById("root").appendChild(level_3_object_2);
        level_3_object_2.onclick=click_3_2;
    }
    
    if(level_3_object_3_is){
        level_3_object_3=document.createElement('button');
        level_3_object_3.style.left="57%";
        level_3_object_3.style.top="70%";
        level_3_object_3.className="btn-question";
        level_3_object_3.id="level_3_object_3";
        level_3_object_3.innerHTML='3 3';
        document.getElementById("root").appendChild(level_3_object_3);
        level_3_object_3.onclick=click_3_3;
    }
    
    if(level_3_object_4_is){
        level_3_object_4=document.createElement('button');
        level_3_object_4.style.left="37%";
        level_3_object_4.style.top="30%";
        level_3_object_4.className="btn-question";
        level_3_object_4.id="level_3_object_4";
        level_3_object_4.innerHTML='3 4';
        document.getElementById("root").appendChild(level_3_object_4);
        level_3_object_4.onclick=click_3_4;
    }
    
    root.innerHTML+=`
    <button style="background-color:white; border-radius:50%; background-position:center; background-size:100% 100%; background-image:url('/back'); width:60px; height:60px; position:fixed; left:10px; bottom:10px;" onclick='createControlCenter()'></button>
    <button onclick="exit_game()" style="background-size:100% 100%;border-radius:50%; background-color:white; background-image:url(/exit); position:fixed; width:60px;height:60px; left:20px;top:20px;font-size: 2rem;"></button>
    `;
    root.innerHTML=root.innerHTML.replace(`"level_3_object_1"`,`"level_3_object_1" onclick="click_3_1()"`);
    root.innerHTML=root.innerHTML.replace(`"level_3_object_2"`,`"level_3_object_2" onclick="click_3_2()"`);
    root.innerHTML=root.innerHTML.replace(`"level_3_object_3"`,`"level_3_object_3" onclick="click_3_3()"`);
    root.innerHTML=root.innerHTML.replace(`"level_3_object_4"`,`"level_3_object_4" onclick="click_3_4()"`);
    createRightPanel();
}
