let zamena={' ':' ',};
let createKeyBoard=()=>{
    let tempText=programmerText;
    let keyBoardChar='';
    for(i in tempText) 
        if ((keyBoardChar.indexOf(tempText[i])==-1) && (tempText[i]!=' '))
            keyBoardChar+=tempText[i];
    console.log(programmerText);
    console.log(keyBoardChar);
    let root=document.getElementById('root');
    if(document.getElementById("keyboard"))
    root.removeChild(document.getElementById("keyboard"));
    
    let keyBoard=document.createElement('div');
    keyBoard.id="keyboard";
    root.appendChild(keyBoard);
    keyBoard.style.width=`${6*100}px`;
    keyBoard.style.height=`${5*50}px`;
    keyBoard.style.left=`calc(50% - ${6*50}px)`;
    keyBoard.style.bottom=0;
    keyBoard.className="keyBoard";
    keyBoard.style.position="absolute";
    for(i in keyBoardChar){
        let btn=document.createElement('button');
        btn.className="btn-char";
        btn.innerHTML=keyBoardChar[i];
        keyBoard.appendChild(btn);
        btn.onclick=()=>{
            let input=document.createElement('div');
            root.appendChild(input);
            input.style.width='600px';
            input.style.height='50px';
            input.style.position='absolute';
            input.style.left=`calc(50% - ${6*50}px)`;
            input.style.bottom='255px';
            let inputBlock=document.createElement('input');
            input.appendChild(inputBlock);
            inputBlock.style.width='550px';
            inputBlock.style.height='50px';
            let inputBtn=document.createElement('button');
            inputBtn.style.width='50px';
            inputBtn.style.height='50px';
            inputBtn.innerHTML='OK';
            inputBlock.style.fontSize='40';
            input.appendChild(inputBtn);
            inputBtn.onclick=()=>{
                zamena[btn.innerHTML]=inputBlock.value;
                temp1.innerHTML=programmerText+'<br>'+'<br>';
                tempText=programmerText;
                for(let t in programmerText){
                    if (zamena[programmerText[t]]!==undefined)
                        temp1.innerHTML+=zamena[programmerText[t]];
                    else
                        temp1.innerHTML+=programmerText[t];
                }
                root.removeChild(input);
            }
        }
    }
    temp1=document.getElementById('programmer');
    temp1.innerHTML=programmerText;
}
let createBagFromProgrammer=()=>{
    close=document.createElement("button");
    close.style.position="absolute";
    close.style.left="5px";
    close.style.top="5px";
    close.style.width="60px";
    close.style.height="60px";
    close.style.borderStyle="none";
    close.style.backgroundColor="RGBA(0,0,0,0)";
    close.style.backgroundImage="url(/exit)";
    close.style.backgroundSize="100% 100%";
    let bag=document.createElement('div');
    let bagobl=document.createElement("div");
    bagobl.id="bag";
    bag.appendChild(bagobl);
    bagobl.style.position="absolute"
    bagobl.style.left="80px";
    bagobl.style.top="80px";
    bagobl.style.width="calc(100% - 100px)"
    bagobl.style.height="calc(100% - 100px)"
    bag.appendChild(close);
    document.body.appendChild(bag);
    close.onclick=()=>{
        document.body.removeChild(bag);
    }
    bag.style.position="absolute";
    bag.style.right="100px";
    bag.style.top="5%";
    bag.style.width="470px";
    bag.style.height="600px";
    bag.style.backgroundColor="grey";
    bag.style.borderRadius="20px";
    getBagFromProgrammer();
};

let createRightPanelFromProgrammer=()=>{
    let createBlock=[createMessage,createBagFromProgrammer,createLupa,createStar]
    let root=document.getElementById('root');
    let programmer=document.getElementById('programmer');
    programmer.style.fontSize='50px';
    programmer.style.overflowY="auto";
    let rightPanel=document.createElement('divddx');
    rightPanel.style.position="fixed";
    rightPanel.style.right="0px";
    rightPanel.style.top="20px";
    rightPanel.style.width="100px";
    rightPanel.style.height="400px";
    rightPanel.style.backgroundColor="rgba(11, 0, 29,0.8)";
    rightPanel.style.borderRadius="20px";
    arrBTN=["message","bag","lupa","star"];
    for(let i in arrBTN){
        let name = arrBTN[i];
        temp=document.createElement('button');
        temp.style.position="absolute";
        temp.style.top=100*i+"px";
        temp.style.width="100px";
        temp.style.height="100px";
        temp.style.backgroundImage=`url(/${name})`;
        temp.style.backgroundSize='75% 75%';
        temp.style.backgroundRepeat="no-repeat";
        temp.style.backgroundPosition="center center";
        temp.style.backgroundColor="RGBA(0,0,0,0)";
        temp.style.border="none";
        temp.onclick=()=>{
            createBlock[i]();
        }
        rightPanel.appendChild(temp);
    }
    root.appendChild(rightPanel);
}

let disbtn=true;
node_level=[
    `<button id="level_1" onclick="level_1()" class="level_1 planet" disabled=true>Омега 1</button>
    `,
    `<button id="level_1" onclick="level_1()" class="level_1 planet">Омега 1</button>
    <button id="level_2" onclick="level_2_1()" class="level_2 planet green" disabled=true>Дельта 1</button>
    `,
    `<button id="level_2" onclick="level_2_1()" class="level_2 planet green">Дельта 1</button>
    <button id="level_3" onclick="level_3_1()" class="level_3 planet blue" disabled=true>Сигма 1</button>
    <button id="level_3" onclick="level_3_2()" class="level_3 planet blue" disabled=true>Сигма 2</button>
    <button id="level_3" onclick="level_3_3()" class="level_3 planet blue" disabled=true>Сигма 3</button>
`,
    `<button id="level_3" onclick="level_3_2()" class="level_3 planet blue">Сигма 2</button>
    <button id="level_4" onclick="level_4()" class="level_4 planet black" disabled=true>Гамма</button>
`
];
let createNavigator=()=>{
    document.getElementById("root").innerHTML=`<div id="Navigator" style="position:absolute; left:20%; top:20%; width:60%; height:60%; background-color:lightgrey;"></div>
    <button class="back" onclick='createControlCenter()'></button>
    <button onclick="exit_game()" style="background-size:100% 100%;border-radius:50%; background-color:white; background-image:url(/exit); position:fixed; width:60px;height:60px; left:20px;top:20px;font-size: 2rem;"></button>
    `
    let navigator=document.getElementById("Navigator");


    let temp=document.createElement("button");
    navigator.appendChild(temp);
    temp.className="navigator-value"
    temp.style.position="absolute";
    temp.style.left=`calc(50% - ${75*3-75*(6)+30}px)`;
    temp.style.top=`calc(50% - 35px)`;
    temp.style.width="75px";
    temp.style.height="70px";
    temp.innerHTML="ок";
    temp.onclick=()=>{
        res="";
        s=Array.from(document.getElementsByClassName("navigator-value"));
        for(let i in s){
            res+=s.innerHTML;
        }
        console.log(res);
        if(res==werb_level[level_game][2]){
            funcEval("createLevel_"+(level_game+1)+"()");
        }
        else{
            if(kol[level_game]<podscazka[level_game].length){
            funcEval("Tip+='<br>"+podscazka[level_game][kol[level_game]]+"<br>'");
            funcEval(`Money-=${kol[level_game]*100}`);
            }
            let t=(kol[level_game]<podscazka[level_game].length)?kol[level_game]:podscazka[level_game].length-1;
            push(podscazka[level_game][kol[level_game]])
            kol[level_game]++;
        }
    }


    for(let i=0;i<6;i++){
        
        let temp1=document.createElement("button");
        navigator.appendChild(temp1);
        temp1.className="navigator-value"
        temp1.style.position="absolute";
        temp1.style.left=`calc(50% - ${75*3-75*(i)+30}px)`;
        temp1.style.top=`calc(50% - 35px)`;
        temp1.style.width="75px";
        temp1.style.height="70px";
        temp1.innerHTML=i;
        
        let temp2=document.createElement("button");
        navigator.appendChild(temp2);
        temp2.innerHTML="+";
        temp2.className="navigator-btn-up"
        temp2.style.position="absolute";
        temp2.style.left=`calc(50% - ${75*3-75*(i)+30}px)`;
        temp2.style.top=`calc(50% - 105px)`;
        temp2.style.width="75px";
        temp2.style.height="70px";
        let add1=()=>{
            temp1.innerHTML=(+temp1.innerHTML + 1)%10;
        }
        temp2.onclick=add1;


        let temp3=document.createElement("button");
        navigator.appendChild(temp3);
        temp3.innerHTML="-";
        temp3.className="navigator-btn-down"
        temp3.style.position="absolute";
        temp3.style.left=`calc(50% - ${75*3-75*(i)+30}px)`;
        temp3.style.top=`calc(50% + 35px)`;
        temp3.style.width="75px";
        temp3.style.height="70px";
        let sub1=()=>{
            temp1.innerHTML=(+temp1.innerHTML +9)%10;
        }
        temp3.onclick=sub1;

    }
    createRightPanel();
}
let createProgrammer=()=>{
    document.getElementById("root").innerHTML=`<div id="programmer" style="position:absolute; left:20%; top:0; width:60%; height:60%; background-color:lightgrey;"></div>
    <button style="background-color:white; border-radius:50%; background-position:center; background-size:100% 100%; background-image:url('/back'); width:60px; height:60px; position:fixed; left:10px; bottom:10px;" onclick='createControlCenter()'></button>
    <button onclick="exit_game()" style="background-size:100% 100%;border-radius:50%; background-color:white; background-image:url(/exit); position:fixed; width:60px;height:60px; left:20px;top:20px;font-size: 2rem;"></button>
    `
    createRightPanelFromProgrammer();
}
let createCaptain=()=>{
    document.getElementById("root").innerHTML=`
    <div  style="width:720px; height:320px; display:flex;flex-direction:column;position:absolute;bottom:100px;left:0;">
    <div style="width:720px; height:160px; display:flex;flex-direction:row;">
        <div id="widget-1" class="widget-chesterna">
        </div>
        <div id="widget-2" class="widget-chesterna">
        </div>
        <div id="widget-3" class="widget-chesterna">
        </div>
        <div id="widget-4" class="widget-chesterna">
        </div>
    </div>
    <div style="width:720px; height:160px; display:flex;flex-direction:row;">
        <div id="widget-5" class="widget-chesterna">
        </div>
        <div id="widget-6" class="widget-chesterna">
        </div>
        <div id="widget-7" class="widget-chesterna">
        </div>
        <div id="widget-8" class="widget-chesterna">
        </div>
    </div>
    </div>
    <div id="richag">
    <div id="nogka"></div>
    <div id="ruchka"></div>
    <div id="calc-input"></div>
    </div>
    <button style="background-color:white; border-radius:50%; background-position:center; background-size:100% 100%; background-image:url('/back'); width:60px; height:60px; position:fixed; left:10px; bottom:10px;" onclick='createControlCenter()'></button>
    <button onclick="exit_game()" style="background-size:100% 100%;border-radius:50%; background-color:white; background-image:url(/exit); position:fixed; width:60px;height:60px; left:20px;top:20px;font-size: 2rem;"></button>
    `
    create_widget(1,"дгцосуза",0)
    create_widget(2,"умациеэл",0)
    create_widget(3,"льдегыар",0)
    create_widget(4,"ьмдлгывф",0)
    create_widget(5,"тждлайцу",1)
    create_widget(6,"б ацузхк",1)
    create_widget(7," йыузкцх",1)
    create_widget(8,"цхк эъуз",1)
    create_code_panel();
    createRightPanel();
}

let createMessage=()=>{
    sms=document.createElement('div');
    sms.style.position="absolute";
    sms.style.left="2%";
    sms.style.top="2%";
    sms.style.width="75%";
    sms.style.height="80%";
    sms.style.backgroundColor="lightgrey";
    sms.style.borderRadius="20px";
    sms.style.fontSize="25px";
    textsms=document.createElement('div');
    sms.appendChild(textsms);
    textsms.style.left="80px";
    textsms.style.top="80px";
    textsms.style.position="absolute";
    textsms.style.width="calc(100% - 150px)";
    textsms.style.height="calc(100% - 150px)";
    textsms.style.overflowY="auto";
    textsms.innerHTML=Text;
    document.body.appendChild(sms);
    close=document.createElement("button");
    close.style.position="absolute";
    close.style.left="10px";
    close.style.top="10px";
    close.style.width="60px";
    close.style.height="60px";
    close.style.borderRadius="50%";
    close.style.backgroundSize="100% 100%";
    close.style.backgroundImage=`url("/exit")`;
    textsms.innerHTML=Text;
    sms.appendChild(close);
    close.onclick=()=>{
        document.body.removeChild(sms);
    }
};
let createBag=()=>{
    close=document.createElement("button");
    close.style.position="absolute";
    close.style.left="5px";
    close.style.top="5px";
    close.style.width="60px";
    close.style.height="60px";
    close.style.borderStyle="none";
    close.style.backgroundColor="RGBA(0,0,0,0)";
    close.style.backgroundImage="url(/exit)";
    close.style.backgroundSize="100% 100%";
    let bag=document.createElement('div');
    let bagobl=document.createElement("div");
    bagobl.id="bag";
    bag.appendChild(bagobl);
    bagobl.style.position="absolute"
    bagobl.style.left="80px";
    bagobl.style.top="80px";
    bagobl.style.width="calc(100% - 100px)"
    bagobl.style.height="calc(100% - 100px)"
    bag.appendChild(close);
    document.body.appendChild(bag);
    close.onclick=()=>{
        document.body.removeChild(bag);
    }
    bag.style.position="absolute";
    bag.style.right="100px";
    bag.style.top="5%";
    bag.style.width="470px";
    bag.style.height="600px";
    bag.style.backgroundColor="grey";
    bag.style.borderRadius="20px";
    getBag();
};

let createLupa=()=>{
    close=document.createElement("button");
    close.style.position="absolute";
    close.style.left="5px";
    close.style.top="5px";
    close.style.width="60px";
    close.style.height="60px";
    close.style.borderStyle="none";
    close.style.backgroundColor="RGBA(0,0,0,0)";
    close.style.backgroundImage="url(/exit)";
    close.style.backgroundSize="100% 100%";
    lupa=document.createElement('div');
    lupa.id="lupa";
    textsms=document.createElement('div');
    lupa.appendChild(textsms);
    textsms.style.left="80px";
    textsms.style.top="80px";
    textsms.style.position="absolute";
    textsms.style.width="calc(100% - 150px)";
    textsms.style.height="calc(100% - 150px)";
    textsms.style.overflowY="auto";
    textsms.innerHTML=Tip;
    lupa.appendChild(close);
    document.body.appendChild(lupa);
    close.onclick=()=>{
        document.body.removeChild(lupa);
    }
    lupa.style.position="absolute";
    lupa.style.right="5%";
    lupa.style.top="5%";
    lupa.style.width="80%";
    lupa.style.height="80%";
    lupa.style.backgroundColor="grey";
    lupa.style.borderRadius="20px";
};
let createStar=()=>{
    close=document.createElement("button");
    close.style.position="absolute";
    close.style.left="5px";
    close.style.top="5px";
    close.style.width="60px";
    close.style.height="60px";
    close.style.borderStyle="none";
    close.style.backgroundColor="RGBA(0,0,0,0)";
    close.style.backgroundImage="url(/exit)";
    close.style.backgroundSize="100% 100%";
    lupa=document.createElement('div');
    lupa.id="lupa";
    m=document.createElement("button");
    document.body.appendChild(m);
    m.id="money";
    m.innerHTML=Money;
    m.disable=true;
    lupa.appendChild(m);
    lupa.appendChild(close);
    document.body.appendChild(lupa);
    close.onclick=()=>{
        document.body.removeChild(lupa);
    }
    lupa.style.position="absolute";
    lupa.style.right="5%";
    lupa.style.top="5%";
    lupa.style.width="80%";
    lupa.style.height="80%";
    lupa.style.backgroundColor="grey";
    lupa.style.borderRadius="20px";
};
let createRightPanel=()=>{
    let createBlock=[createMessage,createBag,createLupa,createStar]
    let root=document.getElementById('root');
    let rightPanel=document.createElement('divddx');
    rightPanel.style.position="fixed";
    rightPanel.style.right="0px";
    rightPanel.style.top="20px";
    rightPanel.style.width="100px";
    rightPanel.style.height="400px";
    rightPanel.style.backgroundColor="rgba(11, 0, 29,0.8)";
    rightPanel.style.borderRadius="20px";
    arrBTN=["message","bag","lupa","star"];
    for(let i in arrBTN){
        let name = arrBTN[i];
        temp=document.createElement('button');
        temp.style.position="absolute";
        temp.style.top=100*i+"px";
        temp.style.width="100px";
        temp.style.height="100px";
        temp.style.backgroundImage=`url(/${name})`;
        temp.style.backgroundSize='75% 75%';
        temp.style.backgroundRepeat="no-repeat";
        temp.style.backgroundPosition="center center";
        temp.style.backgroundColor="RGBA(0,0,0,0)";
        temp.style.border="none";
        temp.onclick=()=>{
            createBlock[i]();
        }
        rightPanel.appendChild(temp);
    }
    root.appendChild(rightPanel);
};
let createControlCenter=()=>{
    document.body.style.backgroundImage="Url(/bodyingame)";
    document.body.style.backgroundSize="100% auto";
    document.getElementById('root').innerHTML=`
    <div class="full_s column">
    <div id='controlCenter' class='full_s row'>
    </div>
    </div>
    <button onclick="exit_game()" style="background-size:100% 100%;border-radius:50%; background-color:white; background-image:url(/exit); position:fixed; width:60px;height:60px; left:20px;top:20px;font-size: 2rem;"></button>
    `;
    createRightPanel();
    socket.emit("get my command roles, avatars and names",player.command);
}
