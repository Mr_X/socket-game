`<style>
       #monitor2{
            transform-origin: center center;
            position:absolute;
            left:35%;
            top:30%;
            width:30%;
            height:30%;
            background-color: green;
            background-image: url('/monitor_2');
            background-size:100% 100%;
            border-style:solid;
            border-color:white;
            border-width:3px;
        }
        #stol2{
            transform-origin: 0 center;
            position:absolute;
            left:35%;
            top:55%;
            width:30%;
            height:30%;
            background-color: greenyellow;
            transform: rotateX(90deg) translateY(-50px);
            background-size:100% 100%;
            border-style:solid;
            border-color:white;
            border-width:3px;
        }
        #root{
            transform-style: preserve-3d; 
            transform: perspective(400px);
            position:absolute;
            left:0;
            top:0;
            width:100%;
            height:100%;
            background-color: black;
        }
    </style>
    <div id="root">
        <div id="stol2"></div>
        <div id="monitor2"></div>
        <div  style="width:720px; height:320px; display:flex;flex-direction:column;position:absolute;bottom:0;">
            <div style="width:720px; height:160px; display:flex;flex-direction:row;">
                <div id="widget-1" class="widget-chesterna">
                </div>
                <div id="widget-2" class="widget-chesterna">
                </div>
                <div id="widget-3" class="widget-chesterna">
                </div>
                <div id="widget-4" class="widget-chesterna">
                </div>
            </div>
            <div style="width:720px; height:160px; display:flex;flex-direction:row;">
                <div id="widget-5" class="widget-chesterna">
                </div>
                <div id="widget-6" class="widget-chesterna">
                </div>
                <div id="widget-7" class="widget-chesterna">
                </div>
                <div id="widget-8" class="widget-chesterna">
                </div>
            </div>
        </div>
        <div id="richag">
            <div id="nogka"></div>
            <div id="ruchka"></div>
        </div>
    </div>
    <script>
        create_widget(1,"abcdefgh",0)
        create_widget(2,"ab defgh",0)
        create_widget(3,"abcde gh",0)
        create_widget(4,"abcdefgh",0)
        create_widget(5,"ab defgh",1)
        create_widget(6,"abcdefgh",1)
        create_widget(7,"abc efgh",1)
        create_widget(8,"abcde gh",1)
        create_code_panel();
    </script>
`