let programmerText='';
Money=2000;
let updateMoney=()=>{
    document.getElementById("Money").innerHTML=Money;
}
Tip="<br>";
document.getElementById("root").style.position='absolute';
document.getElementById("root").style.display='flex';
document.getElementById("root").style.alignItems='center';
document.getElementById("root").style.justifyContent='center';
document.getElementById("root").style.flexDirection='center';
document.getElementById("root").style.width=window.innerWidth+'px';
document.getElementById("root").style.height=window.innerHeight+'px';

let playerName;
let playerRole;
let playerAvatar;
let comands;
let arrAvatars=[];
for(let i=0;i<11;i++){
    arrAvatars.push(`url("/avatar_${i+1}")`);
}
document.body.style.backgroundImage="url('/bodystandart')";
document.body.style.backgroundSize="auto 200%";
document.body.style.backgroundPosition="center center";
document.getElementById('playerAvatarImg').style.backgroundSize="auto 100%";
document.getElementById('playerAvatarImg').style.backgroundPosition="center center";


let exit_game=()=>{
    if(confirm("Вы дествительно желаете выйти?")){
        socket.emit("end game player", playerName);
    }
}

let nextAvatar=()=>{
    avatar=document.getElementById('playerAvatarImg');
    i=arrAvatars.indexOf(avatar.style.backgroundImage);
    avatar.style.backgroundImage=arrAvatars[(i+1)%arrAvatars.length];
}

let predAvatar=()=>{
    avatar=document.getElementById('playerAvatarImg');
    i=arrAvatars.indexOf(avatar.style.backgroundImage);
    avatar.style.backgroundImage=arrAvatars[(i+arrAvatars.length-1)%arrAvatars.length];
}

socket=io(`http://192.168.43.69:4000/`);
// socket=io(`http://127.0.0.1:4000/`);
let createPlayer=()=>{
    name=document.getElementById('playerName').value;
    playerName=name;
    let body={};
    body.name=name;
    body.avatar=document.getElementById('playerAvatarImg').style.backgroundImage;
    body=JSON.stringify(body);
    socket.emit('create player',body);
};

socket.on("create player result",(data)=>{
    if (data=='true'){
        player=new Player(document.getElementById('playerName').value,document.getElementById('playerAvatarImg').style.backgroundImage);
        push('Регистрация успешна');
        createCommands('commands');
    }
    else{
        push('Логин занят');
    }
});

socket.on("unlock input planet true",()=>{
    disbtn=false;
    console.log(level_game);
    let btn=Array.from(document.getElementsByClassName(`level_${level_game+1}`));
    node_level[level_game]=node_level[level_game].replace('disabled=true','')
    for(let i in btn) 
        btn[i].disabled=undefined;
});
let setBag=(obj)=>{
    let body={}
    console.log(player.command);
    let command=player.command;
    body["command"]=command;
    body["obj"]=obj;
    socket.emit("set bag",JSON.stringify(body));
}
let funcEval=(s)=>{
    body={}
    body.name=player.command;
    body.exec=s;
    body=JSON.stringify(body);
    socket.emit("Func eval",body);
}
socket.on("Eval",(exec)=>{
    eval(exec);
});
let getBagFromProgrammer=()=>{
    socket.emit("get bag FP",player.command);
}
socket.on("update bag FP",(data)=>{
    data=JSON.parse(data);
    bag=document.getElementById("bag");
    console.log(bag);
    let i=0;
    let j=0;
    if(bag!=null)
    bag.style.overflowY='auto';
    for(let k in data){
        temp=document.createElement('button');
        temp.style.width="100px";
        temp.style.height="100px";
        temp.style.position="relative"
        temp.style.backgroundImage="url(/sticker)";
        temp.style.backgroundSize="100% 100%";
        temp.style.backgroundPosition="center center";
        temp.style.backgroundRepeat="no-repeat";
        temp.onclick=()=>{
            let temp1=document.getElementById('programmer');
            programmerText=data[k];
            temp1.innerHTML=programmerText;
            createKeyBoard();
        }
        bag.appendChild(temp);
    }
});
let getBag=()=>{
    socket.emit("get bag",player.command);
}
socket.on("update bag",(data)=>{
    data=JSON.parse(data);
    bag=document.getElementById("bag");
    console.log(bag);
    let i=0;
    let j=0;
    if(bag!=null)
    bag.style.overflowY='auto';
    for(let k in data){
        temp=document.createElement('button');
        temp.style.width="100px";
        temp.style.height="100px";
        temp.style.position="relative"
        temp.style.backgroundImage="url(/sticker)";
        temp.style.backgroundSize="100% 100%";
        temp.style.backgroundPosition="center center";
        temp.style.backgroundRepeat="no-repeat";
        temp.onclick=()=>{
            let temp1=document.createElement("div")
            let temp3=document.createElement("div");
            temp1.appendChild(temp3);
            temp3.innerHTML=data[k];
            temp3.overflowY="auto";
            temp3.style.position="absolute";
            temp3.style.left="80px";
            temp3.style.top="80px";
            temp3.style.width="calc(100% -100px)";
            temp3.style.height="calc(100% -100px)";

            temp1.style.width="80%";
            temp1.style.height="80%";
            temp1.style.position="absolute";
            temp1.style.left="5%";
            temp1.style.top="5%";
            temp1.style.backgroundColor="grey";
            temp1.style.fontSize="30px";
            let temp2=document.createElement("button")
            temp2.style.width="75px";
            temp2.style.height="75px";
            temp2.style.position="absolute";
            temp2.style.left="10px";
            temp2.style.top="10px";
            temp2.style.backgroundColor="grey";
            temp2.style.backgroundImage="url(/exit)";
            temp2.style.backgroundSize="100% 100%";
            temp2.style.borderStyle="none";
            temp1.style.borderRadius="25px";
            temp1.appendChild(temp2);
            temp2.onclick=()=>{
                temp1.parentNode.removeChild(temp1);
            }
            document.body.appendChild(temp1);
        }
        bag.appendChild(temp);
    }
});
let level_1=()=>{
    socket.emit('start level 1',player.command);
}

socket.on("start level 1!",()=>{
    createLevel_1();
});

let level_2_1=()=>{
    socket.emit('start level 2 1',player.command);
}

socket.on("start level 2 1!",()=>{
    createLevel_2_1();
});

let level_2_2=()=>{
    socket.emit('start level 2 2',player.command);
}

socket.on("start level 2 2!",()=>{
    createLevel_2_2();
});

let level_2_3=()=>{
    socket.emit('start level 2 3',player.command);
}

socket.on("start level 2 3!",()=>{
    createLevel_2_3();
});

let level_3_1=()=>{
    socket.emit('start level 3 1',player.command);
}

socket.on("start level 3 1!",()=>{
    createLevel_3_1();
});

let level_3_2=()=>{
    socket.emit('start level 3 2',player.command);
}

socket.on("start level 3 2!",()=>{
    createLevel_3_2();
});

let level_3_3=()=>{
    socket.emit('start level 3 3',player.command);
}

socket.on("start level 3 3!",()=>{
    createLevel_3_3();
});

let level_4=()=>{
    socket.emit('start level 4',player.command);
}

socket.on("start level 4!",()=>{
    createLevel_4();
});



let createCommand=()=>{
    name=document.getElementById('commandName').value;
    document.getElementById('commandName').value='';
    let body={};
    player.command=name;
    body.name=name;
    //TODO В тело сообщения надо накидать все что может пригодиться
    body=JSON.stringify(body);
    socket.emit('create command',body);
};

socket.on("update command list",(data)=>{
    // new_meta();
    data=JSON.parse(data);
    let commands=document.getElementById('commandsList');
    if(""+commands=="null")return;
    commands.innerHTML='';
    commands.style.position="absolute";
    commands.style.right="10px";
    commands.style.width='80%';
    commands.style.top='80px';
    commands.style.height='80%';
    let flag=false;
    for(let i in data){
        for(j in data[i].players) {
            if (j==playerName) flag=true;
        }
    }
    for(let i in data){
        let n=0; 
        for(j in data[i].players) n++;
        let block=document.createElement('div');
        block.style.width='100%';
        let text=document.createElement('button');
        text.disabled=true;
        text.style.width="calc(100% - 80px)";
        text.style.height="60px";
        text.style.fontSize="25px";
        text.className="commandLine";
        text.innerHTML=`Команда ${i}, ${n} из 3 в команде`;
        block.appendChild(text);
        if(!((flag)||(n==3))){
            let add=document.createElement('button');
            add.innerHTML="+";
            add.className="add";
            add.onclick=()=>{
                player.command=i;
                socket.emit('add to command',i);
            }
            block.appendChild(add);
        }
        commands.appendChild(block);
    }
});

socket.on("sms",(data)=>{
    push(data);
    //console.log(data);
});

socket.on("create command result",(data)=>{
    if (data=='true'){
        push('Регистрация команды успешна');
        socket.emit("get command list");
    }
    else{
        push('Имя команды занято');
    }
    socket.emit("get command list");
});
socket.on("start game all users",()=>{
    createLor();
});

socket.on("add to command result",(data)=>{
    //TODO нуден объект-персонаж, напишу класс
    if (data=='true'){
        push('Подготовительный этап');
        createLobi();
    }
    else{
        push('Привышено число игроков в команде');
    }
    socket.emit("get command list");
});

socket.on("your command roles, avatars and names",(data)=>{
    data=JSON.parse(data);
    while(!document.getElementById("controlCenter")){};
    self=document.getElementById("controlCenter");
    self.innerHTML='';
    self.style.position="absolute";
    self.style.width="680px";
    self.style.height="280px";
    self.style.left="calc(50% - 340px)";
    self.style.top="calc(74% - 140px)";
    for(let i in data)
        if(playerName==data[i].name)
            playerRole=data[i].role;
    let j=0;
    for(let i in data){
        let temp=document.createElement("div");
        let tempA=document.createElement("div")
        tempA.style.backgroundImage=data[i].avatar;
        let tempN=document.createElement("button");
        tempN.innerHTML=data[i].role;
        if(playerName==data[i].name) playerRole==data[i].role; 
        self.appendChild(temp);
        temp.appendChild(tempA);
        temp.appendChild(tempN);
        temp.style.backgroundColor="f4efec";
        
        tempA.onclick=()=>{
            if(data[i].role=="Капитан"){
                    createCaptain()
            }
            
            if(data[i].role=="Штурман"){
                    createNavigator()
            }

            if(data[i].role=="Программист"){
                    createProgrammer()
            }
        };
        tempN.onclick=()=>{
            if(data[i].role=="Капитан"){
                    createCaptain()
            }
            
            if(data[i].role=="Штурман"){
                    createNavigator()
            }

            if(data[i].role=="Программист"){
                    createProgrammer()
            }
        };

        temp.style.position="relative";
        temp.style.width="240px";
        temp.style.height="240px";
        temp.style.backgroundColor='rgba(0,0,0,0)';
        j++;
        
        tempA.style.width="200px"
        tempA.style.height="200px"
        tempA.style.position="absolute";
        tempA.style.borderRadius="50%";
        tempA.style.backgroundSize="auto 100%";
        tempA.style.backgroundRepeat="no-repeat";
        tempA.style.backgroundPosition="center center";
        tempA.style.backgroundColor="f4efec";
        tempA.style.top=0;
        tempA.style.left="20px";
        
        tempN.style.position="absolute";
        tempN.style.width="200px";
        tempN.style.height="40px";
        tempN.style.backgroundColor="f4efec";
        tempN.style.color="black";
        tempN.style.border="none";
        tempN.style.borderRadius="20px";
        tempN.style.fontSize="20px";
        tempN.style.bottom=0;
        tempN.style.left="20px";
    }
    console.log(player);
});

socket.on("your command avatars and names",(data)=>{
    data=JSON.parse(data);
    self=document.getElementById("playersLobi");
    self.innerHTML='';
    self.style.display='flex';
    let j=0;
    for(let i in data){
        console.log(i);
        console.log(data[i]);
        
        let temp=document.createElement("div");
        let tempA=document.createElement("div")
        tempA.style.backgroundImage=data[i];
        let tempN=document.createElement("button");
        tempN.innerHTML=i;
        self.appendChild(temp);
        temp.appendChild(tempA);
        temp.appendChild(tempN);
        // temp.style.backgroundColor="f4efec";
        
        j++;
        temp.style.position="relative";
        // temp.style.left="calc("+j*33 + "%" - "75px)";
        temp.style.width="300px";
        temp.style.height="390px";
        tempA.style.position="absolute";
        tempA.style.margin="10px 0 10px 0";
        tempA.style.width="280px";
        tempA.style.height="280px";
        tempA.style.borderRadius="150px";
        tempA.style.backgroundSize="auto 100%";
        tempA.style.backgroundRepeat="no-repeat";
        tempA.style.backgroundPosition="center center";
        tempA.style.backgroundColor="f4efec";
        tempA.style.top=0;
        tempA.style.border="none";
        
        tempN.style.position="absolute";
        tempN.style.width="180px";
        tempN.style.height="90px";
        tempN.style.left="calc(50% - 90px)"
        tempN.style.backgroundColor="rgba(0,0,0,0)";
        tempN.style.color="f4efec"
        tempN.style.border="none";
        tempN.style.fontSize=((10>(54-2*player.name.length))?10:(54-2*player.name.length)) +"px";
        tempN.style.bottom="0%";
    }
});