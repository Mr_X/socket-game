let server = require('express')();
let http = require('http').Server(server);
let io = require('socket.io')(http);

const roles=["Капитан","Программист","Штурман"];

const cors = require('cors');
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({extended: false});
const jsonParser = bodyParser.json();

let commands={};
let players={};
let sockets={};
let bag={}

server.use(cors());
server.use(urlencodedParser);
server.use(jsonParser);

server.get('/',(req,res)=>{
    res.sendFile(__dirname+'/index.html');
});
server.get('/comands',(req,res)=>{
    res.sendFile(__dirname+'/comands.js');
});
server.get('/push',(req,res)=>{
    res.sendFile(__dirname+'/push.js');
});
server.get('/lor',(req,res)=>{
    res.sendFile(__dirname+'/lor.js');
});
server.get('/index',(req,res)=>{
    res.sendFile(__dirname+'/index.js');
});

server.get('/your_place',(req,res)=>{
    res.sendFile(__dirname+'/your_place.html');
});
for(let i=1;i<=12;i++)
server.get(`/avatar_${i}`,(req,res)=>{
    res.sendFile(__dirname+`/image/avatars/${i}.jpg`);
});

server.get(`/controlCenter`,(req,res)=>{
    res.sendFile(__dirname+`/controlCenter.js`);
});

server.get(`/captain`,(req,res)=>{
    res.sendFile(__dirname+`/captain.js`);
});

server.get('/bodystandart',(req,res)=>{
    res.sendFile(__dirname+'/image/body/standart.jpg');
});
server.get('/bodyingame',(req,res)=>{
    res.sendFile(__dirname+'/image/body/standart.png');
});
for(let i=1;i<6;i++)
server.get(`/t${i}`,(req,res)=>{
    res.sendFile(__dirname+`/image/space/t${i}.jpg`);
});

for(let i=0;i<3;i++)
server.get(`/monitor_${i}`,(req,res)=>{
    res.sendFile(__dirname+`/image/monitors/${i}.gif`);
});

server.get('/main_style',(req,res)=>{
    res.sendFile(__dirname+'/style/main.css');
});

server.get('/html',(req,res)=>{
    res.sendFile(__dirname+'/html.html');
});
server.get('/main_class',(req,res)=>{
    res.sendFile(__dirname+'/main.js');
});
server.get('/lobi',(req,res)=>{
    res.sendFile(__dirname+'/lobi.js');
});
server.get('/navigator',(req,res)=>{
    res.sendFile(__dirname+'/navigator.js');
});
server.get('/programmer',(req,res)=>{
    res.sendFile(__dirname+'/programmer.js');
});
server.get('/startGame',(req,res)=>{
    res.sendFile(__dirname+'/start.html');
});




server.get('/level_1',(req,res)=>{
    res.sendFile(__dirname+'/level_1.js');
});
server.get('/level_2_1',(req,res)=>{
    res.sendFile(__dirname+'/level_2_1.js');
});
server.get('/level_2_2',(req,res)=>{
    res.sendFile(__dirname+'/level_2_2.js');
});
server.get('/level_2_3',(req,res)=>{
    res.sendFile(__dirname+'/level_2_3.js');
});
server.get('/level_3_1',(req,res)=>{
    res.sendFile(__dirname+'/level_3_1.js');
});
server.get('/level_3_2',(req,res)=>{
    res.sendFile(__dirname+'/level_3_2.js');
});
server.get('/level_3_3',(req,res)=>{
    res.sendFile(__dirname+'/level_3_3.js');
});
server.get('/level_4',(req,res)=>{
    res.sendFile(__dirname+'/level_4.js');
});


server.get('/skitala',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/skitala.png');
});
server.get('/question',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/question.png');
});

server.get('/back',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/back.png');
});
server.get('/sv',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/svitok.png');
});
server.get('/exit',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/exit.png');
});
server.get('/star',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/star.svg');
});
server.get('/bag',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/bag.svg');
});
server.get('/lupa',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/lupa.svg');
});
server.get('/message',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/message.svg');
});
server.get('/table',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/table.jpg');
});
server.get('/sticker',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/sticker.png');
});

server.get('/arrowRight',(req,res)=>{
    res.sendFile(__dirname+'/image/icons/right arrow icon.svg');
});

server.post('/',(req,res)=>{
    //console.log("ERROR! POST FOR '/' BLOCK!!!");
});

http.listen(4000,()=>{
    //console.log("127.0.0.1: 4000");
});

io.on('connection',(socket)=>{
    socket.on('message',(data)=>{
        socket.emit('new message',JSON.stringify(data));
    });
    
    socket.on('set bag',(data)=>{
        data=JSON.parse(data);
        let name=data.command;
        let string=data.obj;
        if(bag[name]===undefined)bag[name]={};
        bag[name].push(string);
        console.log(bag[name]);
        console.log(JSON.stringify(bag[name]));
        result=JSON.stringify(bag[name]);
        console.log(name);
        for (let i in commands[name].players){
            sockets[commands[name].players[i]].emit("update bag",result);
        }
    });
    
    socket.on('Func eval',(body)=>{
        body=JSON.parse(body);
        name=body.name;
        exec=body.exec;
        for (let i in commands[name].players){
            sockets[commands[name].players[i]].emit("Eval",exec);
        }
    });
    
    socket.on('get bag',(name)=>{
        result=JSON.stringify(bag[name]);
        for (let i in commands[name].players){
            sockets[commands[name].players[i]].emit("update bag",result);
        }
    });
    
    socket.on('get bag FP',(name)=>{
        result=JSON.stringify(bag[name]);
        for (let i in commands[name].players){
            sockets[commands[name].players[i]].emit("update bag FP",result);
        }
    });

    socket.on('create command',(body)=>{
        body=JSON.parse(body);
        let name=body.name;
        bag[name]=[];
        let flag=true;
        if (name=='') return socket.emit('create command result','false');
        for(let i in commands){
            if (i==name)
                flag=false;
        }
        if (flag && (commands[name]===undefined)){    
            commands[name]={};
            commands[name].size=3;
            commands[name].players={};
            socket.emit('create command result','true');
        }
        else{
            socket.emit('create command result','false');
        }
        //console.log(socket.id);
    });
    socket.on("add to command",(name)=>{
        commands[name].players[players[socket.id].name]=socket.id;
        socket.emit("add to command result","true");
        io.emit('update command list',JSON.stringify(commands));
    });
    socket.on("delete to command",(name)=>{
        let temp=commands[name].players;
        delete temp[players[socket.id].name];
        commands[name].players=temp;
        console.log(commands[name]);
        socket.emit("delete to command result","true");
        io.emit('update command list',JSON.stringify(commands));
    });

    socket.on("start game",(comandsName)=>{
        let temp=0;
        for(let i in commands[comandsName].players){
            temp++;
        }
        console.log(temp)
        if(temp==3)
            for (let i in commands[comandsName].players){
                sockets[commands[comandsName].players[i]].emit("start game all users");
            }
        else socket.emit("sms",'не полный состав');
    });


    socket.on("get my command roles, avatars and names",(comandsName)=>{
        result={}
        j=0;
        for (let i in commands[comandsName].players){
            let id=commands[comandsName].players[i];
            players[id].role=roles[j % 3];
            result[players[id].name]={"avatar":players[id].avatar,"role":players[id].role,"name":players[id].name};
            j++;
        }
        console.log(result);
        result=JSON.stringify(result);
        socket.emit("your command roles, avatars and names",result);
    });

    socket.on("get my command avatars and names",(comandsName)=>{
        result={}
        j=0;
        for (let i in commands[comandsName].players){
            let id=commands[comandsName].players[i];
            result[players[id].name]=players[id].avatar;
            players[id].role=roles[j % 3];
            j++;
        }
        result=JSON.stringify(result);
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("your command avatars and names",result);
        }
    });

    socket.on("get command list",(data)=>{
        io.emit('update command list',JSON.stringify(commands));
    });

    socket.on('create player',(body)=>{
        body=JSON.parse(body);
        let name=body.name;
        let avatar=body.avatar;
        if (name=='') return socket.emit('create player result','false');
        let flag=true;
        for(let i in players){
            if (players[i].name==name)
                flag=false;
        }
        if (flag && (players[socket.id]===undefined)){    
            players[socket.id]={};
            players[socket.id].socket=socket;
            players[socket.id].name=name;
            players[socket.id].avatar=avatar;
            sockets[socket.id]=socket;
            socket.emit('create player result','true');
        }
        else{
            socket.emit('create player result','false');
        }
        //console.log(socket.id);
    });
    socket.on("unlock input planet",(comandsName)=>{
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("unlock input planet true");
        }
    });

    socket.on("delete player",()=>{
        delete players[socket.id];
    });


    socket.on("start level 1",(comandsName)=>{
        for (let i in commands[comandsName].players){
            console.log(commands[comandsName].players[i]);
            sockets[commands[comandsName].players[i]].emit("start level 1!");
        }
    });
    socket.on("start level 2",(comandsName)=>{
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("start level 2!");
        }
    });
    socket.on("start level 2 2",(comandsName)=>{
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("start level 2 2!");
        }
    });
    socket.on("start level 2 3",(comandsName)=>{
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("start level 2 3!");
        }
    });
    socket.on("start level 3 1",(comandsName)=>{
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("start level 3 1!");
        }
    });
    socket.on("start level 3 2",(comandsName)=>{
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("start level 3 2!");
        }
    });
    socket.on("start level 3 3",(comandsName)=>{
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("start level 3 3!");
        }
    });
    socket.on("start level 4",(comandsName)=>{
        for (let i in commands[comandsName].players){
            sockets[commands[comandsName].players[i]].emit("start level 4!");
        }
    });

});
