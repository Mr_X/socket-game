let close_push=(div)=>{
    //console.log(''+div.parentNode);
    if(div.parentNode===undefined) return;
    if(""+div.parentNode=="null") return;
    //console.log(div);
    div.parentNode.removeChild(div);
}

let push=(data)=>{
    let div=document.createElement('button');
    div.innerHTML=data;
    div.style.position='absolute';
    div.style.right=0;
    div.id='push';
    div.style.transition='0.5s';
    div.style.bottom='-70px';
    div.style.background='grey';
    div.style.height='70px';
    div.style.width='150px';
    close=document.createElement('button');
    close.style.position='absolute';
    close.style.right=0;
    close.style.top=0;
    close.style.width='20px';
    close.style.height='20px';
    close.style.background='rgba(0,0,0,0)';
    close.style.border='none';
    close.innerHTML='X';
    div.appendChild(close);
    document.body.appendChild(div);
    close.onclick=()=>{close_push(div)};
    if (div.parentNode===undefined)return;
    let i;
    i=setInterval(()=>{
        if (div.style!==undefined)
            div.style.bottom=0;
    },500);
    let j;
    j=setInterval(()=>{
        clearInterval(j)
        close_push(div);
        div=false;
    },5000);
}
