let level_game=0;

let werb_level=[
    ["альфа   " ,"1961" ,"999999999"],
    ["энигма  " ,""     ,"999999999"],
    ["Кита    " ,""     ,"004335"],
    ["Тау     " ,""     ,"004211"]
];

let create_code_panel=()=>{
    let richag=document.getElementById("richag");
    richag.style.width="30%";
    richag.style.height="200px";
    richag.style.right=0;
    richag.style.bottom="60%";
    richag.style.position="absolute";
    richag.style.marginRight="90px";
    richag.style.transition="2s";

    let ruchka=document.getElementById("ruchka");
    ruchka.style.backgroundColor="grey";
    ruchka.style.height="50px";
    ruchka.style.width="90%";
    ruchka.style.left="5%";
    ruchka.style.top="0";
    ruchka.style.position="absolute";

    let nogka=document.getElementById("nogka");
    nogka.style.backgroundColor="white";
    nogka.style.height="95%";
    nogka.style.width="40%";
    nogka.style.left="30%";
    nogka.style.bottom="0";
    nogka.style.position="absolute";

    let calc=document.getElementById("calc-input");
    root.appendChild(calc);
    calc.style.position='absolute';
    calc.style.width="25%";
    calc.style.marginRight="90px";
    calc.style.height="30%";
    calc.style.right=0;
    calc.style.bottom="25%";

    let input=document.createElement("div");
    input.id="calc-code";
    calc.appendChild(input);
    input.style.height="20%";
    input.style.width="90%";
    input.style.backgroundColor="white";
    input.style.top=0;
    input.style.fontSize="20px";
    input.style.left=0;

    let cvadr=["123","456","789",["X","0","ok"]];
    for(i in cvadr)
        for(j in cvadr[i]){
            let temp=document.createElement("button");
            calc.appendChild(temp);
            temp.innerHTML=cvadr[i][j];
            temp.style.fontSize="20px";
            temp.style.width=`${90/3}%`;
            temp.style.height=`${90/3}%`;
            temp.style.left=`${90*j/3}%`;
            temp.style.top=`${20+(i+1)*20}%`;
            temp.onclick=()=>{
                if((temp.innerHTML!="X")&&(temp.innerHTML!="ok"))
                input.innerHTML+=temp.innerHTML;
                else if(temp.innerHTML=="X"){
                    input.innerHTML="";
                }
                else {
                    test();
                }
            }
        }
}

let create_widget=(number,str,row)=>{
    let widget=document.getElementById("widget-"+number);
    widget.style.width="160px";
    widget.style.height="160px";
    widget.style.backgroundColor="grey";
    widget.style.borderRadius="50%";
    widget.style.position="relative";
    let btn=document.createElement('button');
    btn.innerHTML=str[0];
    btn.style.position="absolute";
    btn.style.width="50px";
    btn.style.height="50px";
    btn.style.left=`55px`;
    btn.style.backgroundColor="white";
    btn.style.borderRadius="50%";
    btn.disabled=true;
    btn.id="widget-"+number+"-center"
    btn.style.color='black';
    btn.style.fontSize="30px";
    btn.style.top=`55px`;
    btn.style.border='none';
    widget.appendChild(btn);
    for(let i=0;i<8;i++){
        let temp=document.createElement('button')
        temp.innerHTML=str[i];
        temp.style.position="absolute";
        temp.style.width="40px";
        temp.style.height="40px";
        temp.style.left=`${60+60*Math.sin(2*Math.PI*i/8)}px`;
        temp.style.top=`${60+60*Math.cos(2*Math.PI*i/8)}px`;
        temp.style.fontSize="30px";
        temp.style.color='black';
        temp.style.backgroundColor="lightgrey";
        temp.style.borderRadius="50%";
        temp.style.border='none';
        temp.onclick=()=>{
            document.getElementById("widget-"+number+"-center").innerHTML=temp.innerHTML;
        }
        widget.appendChild(temp);
    }
}


let kol=[0,0,0,0];

let podscazka=[
    [
    "Первый полет человека в космос",
    "1961"
    ],[
    "Используйте шифр Цезаря.",
    "Нацистская Германия во время Второй мировой войны использовала электромеханическая шифровальная машина. Взломать ее помогла небольшая неосторожность при отправке сообщений.",
    "шифр Цезаря с ключом 2.",
    "Кодовое слово: «Энигма»."
    ],[
    "Для шифрования текста использовалась формула (A*x+B) mod M = y, где x-номер буквы исходного текста, y-номер буквы зашифрованного текста, M-количество букв в алфавите. Для расшифровки проделайте действия в обратном порядке.",
    "Если номер буквы — четное число, то ее номер разделите на 2, иначе, к ее номеру прибавьте 33, затем разделите на 2.",
    "Например, для расшифровки «Ь» посмотрим в таблицу, буква стоит на 30-м месте. 30:2=15. Следовательно, искомая буква стоит на 15-м месте. Это «Н»."
    ],[
    "Частотный анализ",
    "Воспользуйся таблицой"
    ]
]

let test=()=>{
    let werb='';
    for(i =1; i<=8; i++)
    werb+=document.getElementById("widget-"+i+"-center").innerHTML;
    if((werb==werb_level[level_game][0])&&(document.getElementById("calc-code").innerHTML==werb_level[level_game][1])){
        funcEval("createLevel_"+(level_game+1)+"()");
        let richag=document.getElementById("richag");
        richag.style.height="70px";
    }
    else{
        if(kol[level_game]<podscazka[level_game].length){
        funcEval("Tip+='<br>"+podscazka[level_game][kol[level_game]]+"<br>'");
        funcEval(`Money-=${kol[level_game]*100}`);
        }
        let t=(kol[level_game]<podscazka[level_game].length)?kol[level_game]:podscazka[level_game].length-1;
        push(podscazka[level_game][kol[level_game]])
        kol[level_game]++;
    }
}
